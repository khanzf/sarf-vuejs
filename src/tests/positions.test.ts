import { test, expect } from "vitest";
import { doSarf } from "./../sarf";

let faa = "ف";
let ayn = "ع";
let lam = "ل";

test("Position checks", () => {
	expect(0).toEqual(0);
	let forms = doSarf(faa, ayn, lam);

	for (const form in forms) {
		for (const index in forms[form]) {
			let wordset = forms[form][index];
			for (const i in wordset.faa_locations) {
				let p = wordset.faa_locations[i];
				console.log("form:", form, "mushtaq:", index, "faa_position:", p);
				expect(wordset.word[p]).toEqual(faa);
			}
			for (const i in wordset.ayn_locations) {
				let p = wordset.ayn_locations[i];
				console.log("form:", form, "mushtaq:", index, "ayn_position:", p);
				expect(wordset.word[p]).toEqual(ayn);
			}
			for (const i in wordset.lam_locations) {
				let p = wordset.lam_locations[i];
				console.log("form:", form, "mushtaq:", index, "lam_position:", p);
				expect(wordset.word[p]).toEqual(lam);
			}
		}
	}
});
