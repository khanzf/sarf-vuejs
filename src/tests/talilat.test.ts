import { test, expect } from "vitest";
import { WordsetBase } from "./../sarf";
const unittests = require('./tests.json');

test("Talilat Checks", () => {
	for (let i = 0; i < unittests.length; i++) {
		let row = unittests[i];
		console.log("test #", i, "word:", row["name"]);
		let faa = row["faa_kalima"];
		let ayn = row["ayn_kalima"];
		let lam = row["lam_kalima"];
		let mushtaq = row["mushtaq"];
		let form = row["form"];
		let wordset = new WordsetBase(faa, ayn, lam, mushtaq, form);
		wordset.word = row["inputword"];
		wordset.munsarif = row["munsarif"];
		wordset.faa_locations = row["faa_locations"];
		wordset.ayn_locations = row["ayn_locations"];
		wordset.lam_locations = row["lam_locations"];
		wordset.gender = row["gender"];
		wordset.form = row["form"];
		wordset.talilat();
		expect(wordset.word).toEqual(row["expectedword"]);
	}
});
