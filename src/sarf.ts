function replaceAt(start: string, index: number, replacement: string) {
  return start.substring(0, index) + replacement + start.substring(index + replacement.length);
}

function deleteAt(start: string, index: number) {
  return start.substring(0, index) + start.substring(index+1);
}

function insertAt(start: string, index: number, insertion: string) {
  return start.substring(0, index) + insertion + start.substring(index);
}

const FATHA = "\u064E";
const DHAMMA = "\u064F";
const KASRA = "\u0650";

const FATHATAYN = "\u064B";
const DHAMMATAYN = "\u064C";
const KASRATAYN = "\u064D";

const SUKOON = "\u0652";
const SHADDA = "\u0651";

const ALIF = "\u0627";
const TAH = "\u062A";
const TAHMARBUTA = "\u0629";
const HAMZA = "\u0621";
const HAMZAALIF = "\u0623";
const HAMZAALIFBOT = "\u0625";
const HAMZAWAOW = "\u0624";
const HAMZAYAH = "\u0626";
const RAA = "\u0631";
const ALIFMAQSURA = "\u0649";

const YAH = "\u064A";
const SIN = "\u0633";
const WAOW = "\u0648";
const MEEM = "\u0645";
const NUN = "\u0646";

export class WordsetBase {
  faa: string;
  ayn: string;
  lam: string;
  mushtaq: string; // Things like hazir_madi, hazir_mudari
  word: string; // Text of the word
  form: string; // Nasara, Qaatala, Ijtanaba, etc
  munsarif: string;
  ruleset: string; // Which rules are enabled
  gender: boolean;
  matchpath: string;

  faa_locations: Array<number>;
  ayn_locations: Array<number>;
  lam_locations: Array<number>;

  constructor(faa: string, ayn: string, lam: string, mushtaq: string, form: string) {
    this.faa = faa;
    this.ayn = ayn;
    this.lam = lam;
    this.word = "";
    this.form = "";
    this.ruleset = "optional";
    this.form = form;

    this.faa_locations = [];
    this.ayn_locations = [];
    this.lam_locations = [];
    this.munsarif = "munsarif";
    this.matchpath = "";

    this.mushtaq = mushtaq;

    this.gender = true; // True = Masculine, False = Feminine
  }

  replaceHamza() {
    let hamzacount = (this.faa == HAMZA ? 1 : 0) +
      (this.ayn == HAMZA ? 1 : 0) +
      (this.lam == HAMZA ? 1 : 0);
  
    if (hamzacount == 0) {
      return;
    }
  
    // Beginning Hamza
    if (this.faa == HAMZA) {
      let beginningHamzaRegEx = /^\u0621([\u064E\u064F\u0650])/;
      let beginningHamzaMatch = beginningHamzaRegEx.exec(this.word);
  
      if (beginningHamzaMatch) {
        let originalword = this.word;
        if (beginningHamzaMatch[1] === FATHA || beginningHamzaMatch[0] == DHAMMA) { // Fatha
          this.word = replaceAt(this.word, 0, HAMZAALIF);
        } else if (beginningHamzaMatch[1] === KASRA) {
          this.word = replaceAt(this.word, 0, HAMZAALIFBOT);
        }
        this.matchpath += "|Hz:" + this.word + "→" + originalword;
        hamzacount = hamzacount - 1;
      }
    }
  
    // Any other Hamza
    for (let i = 0; i < hamzacount; i++) {
      let hamzaMatch = /\u0621([\u064E\u064F\u0650\u0652])/;
      let matcher = hamzaMatch.exec(this.word);
      if (matcher) {
        let originalword = this.word;
        if (matcher[1] == FATHA) {
          this.word = replaceAt(this.word, matcher.index, HAMZAALIF);
        } else if (matcher[1] == DHAMMA) {
          this.word = replaceAt(this.word, matcher.index, HAMZAWAOW);
        } else if (matcher[1] == KASRA) {
          this.word = replaceAt(this.word, matcher.index, HAMZAYAH);
        } else if (matcher[1] == SUKOON) {
          let previous_haraka = this.word[matcher.index-1];
          switch(previous_haraka) {
            case FATHA:
              this.word = replaceAt(this.word, matcher.index, HAMZAALIF);
              break;
            case DHAMMA:
              this.word = replaceAt(this.word, matcher.index, HAMZAWAOW);
              break;
            case KASRA:
              this.word = replaceAt(this.word, matcher.index, HAMZAYAH);
              break;
          }
        }
        this.matchpath += "|Hz:" + this.word + "→" + originalword;
      }
    }
  }

  mahmuz_rule2() {
  const rule1regex = /[\u0621\u0623-\u0626]([\u064E-\u0650])[\u0623-\u0627](\u0652|[\u0628-\u064A]|$)/;
  var match = rule1regex.exec(this.word);

  if (match) {
    const originalword = this.word;
    if (match[1] == "\u064E") { // Fatha condition
      // Only remove the sukoon in the Alif condition
      if (match[2] === "\u0652") {
        this.word = deleteAt(this.word, match.index+3);
      }
      this.word = deleteAt(this.word, match.index);
      this.word = deleteAt(this.word, match.index);
      this.word = replaceAt(this.word, match.index, "\u0622");
    } else if (match[1] == "\u064F") { // Dhamma
      this.word = replaceAt(this.word, match.index+2, "\u0648");
    } else if (match[1] == "\u0650") { // Kasra
      this.word = replaceAt(this.word, match.index+2, "\u064A");
    }
    this.matchpath += "|Ma2:" + this.word + "→" + originalword;
  }
}

  mahmuz_rule3() {
    // Rule 3.1
    const rule3regex = /(\u064F|\u0650)[\u0621\u0623-\u0626]\u064E/;
    const match3 = rule3regex.exec(this.word);

    if (match3) {
      const originalword = this.word;
      if (match3[1] == DHAMMA) {
        this.word = replaceAt(this.word, match3.index + 1, WAOW);
        this.matchpath += "|Ma3.1:" + this.word + "→" + originalword;
      } else if(match3[1] == KASRA) {
        this.word = replaceAt(this.word, match3.index + 1, YAH);
        this.matchpath += "|Ma3.2:" + this.word + "→" + originalword;
      }
    }
  }

  mahmuz_rule4() {
    // Rule 4.1
    const rule41regex = /([\u0621\u0623-\u0626]\u0650[\u0621\u0623-\u0626][\u064E-\u0650])|([\u0621\u0623-\u0626][\u064E-\u0650][\u0621\u0623-\u0626]\u0650)/;
    const match41 = rule41regex.exec(this.word);

    if (match41) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match41.index + 2, YAH);
      this.matchpath += "|Ma4.1:" + this.word + "→" + originalword;
    }

    // Rule 4.2
    const rule42regex = /([\u0621\u0623-\u0626]\u064F[\u0621\u0623-\u0626][\u064E-\u0650])|([\u0621\u0623-\u0626][\u064E-\u0650][\u0621\u0623-\u0626]\u064F)/;
    const match42 = rule42regex.exec(this.word);
    if (match42) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match42.index + 2, WAOW);
      this.matchpath += "|Ma4.2:" + this.word + "→" + originalword;
    }
  }

  mahmuz_rule7() {
    const rule7regex = /[\u0652][\u0623-\u0626]([\u064E-\u0650])/;
    var mymatch = rule7regex.exec(this.word);
    
    if (mymatch) {
      const originalword = this.word;
      this.word = deleteAt(this.word, mymatch.index);
      this.word = deleteAt(this.word, mymatch.index);
      this.matchpath += "|Ma7:" + this.word + "→" + originalword;
    }
  }

  mahmuz_rule8() {
  //function mahmuz_rule8(wordset: { [x: string]: { deleteAt: (arg0: number) => any; }; }, this.mushtaq: string, this.faa: string, this.ayn: string, this.lam: string) {
    if (this.faa == RAA && this.ayn == HAMZA && this.lam == YAH && (this.mushtaq === 'hazir_mudari' || this.mushtaq === 'majhul_mudari')) {
      this.matchpath += "|Ma8→Ma7";
      this.mahmuz_rule7();
      // This section terminates a trailing dhamma
      const lastcharlocation = this.word.length-1;
      if (this.word[lastcharlocation] == "\u064F") {
        this.word = deleteAt(this.word, lastcharlocation);
      }
    }
  }

  mahmuz() {

    // Rule 1 - Optional
    if (this.ruleset == "optional") {

    }
    // Rule 2 - Mandatory
    this.mahmuz_rule2();

    if (this.ruleset == "optional") {
      // Rule 3 - Optional
      this.mahmuz_rule3();
      // Rule 4 - Optional
      this.mahmuz_rule4();
    }
    // Rule 5
    // Rule 6
    // Rule 7
    if (this.ruleset == "optional") {
      this.mahmuz_rule7();
    }
    // Rule 8
    this.mahmuz_rule8();
    // Rule 9
    // Rule 10
  }

  muatil_rule1() {

    // Only match for Mudari
    if (this.mushtaq != "hazir_mudari" && this.mushtaq != "majhul_mudari") {
      return;
    }

    // Matches: Beginning, all types of alamat mudhari, then fatha, then waow
    const rule1regex = /^[\u0623\u062A\u064A\u0646]\u064E\u0648/;
    var mymatch = rule1regex.exec(this.word);

    if (mymatch) {
      const originalword = this.word;
      this.word = deleteAt(this.word, 2);
      this.word = deleteAt(this.word, 2);
      this.matchpath += "|Mu1:" + this.word + "→" + originalword;;
    }
  }

  muatil_rule3() {
    // Rules 3.1, 3.2, 3.3 and 3.4
    const rule31regex = /\u0650\u0648\u0652/;
    const rule32regex = /\u064F\u064A\u0652/;
    const rule33regex = /\u064F\u0627/;
    const rule34regex = /\u0650\u0627/;

    var match31 = rule31regex.exec(this.word);
    if (match31) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match31.index+1, "\u064A");
      this.matchpath += "|Mu3.1:" + this.word + "→" + originalword;;
    }

    // Need exception for مُيِّزٌ
    var match32 = rule32regex.exec(this.word);
    if (match32) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match32.index+1, "\u0648");
      this.matchpath += "|Mu3.2:" + this.word + "→" + originalword;
    }

    var match33 = rule33regex.exec(this.word);
    if (match33) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match33.index+1, "\u0648");
      this.matchpath += "|Mu3.3:" + this.word + "→" + originalword;
    }

    var match34 = rule34regex.exec(this.word);
    if (match34) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match34.index+1, "\u064A");
      this.matchpath += "|Mu3.4:" + this.word + "→" + originalword;
    }
  }

  muatil_rule4() {
    if (this.form == "ijtanaba") {
      if (this.faa == "\u0648" || this.faa == "\u064A") {
        const originalword = this.word;
        this.word = replaceAt(this.word, 2, "\u062A");
        this.matchpath += "|Mu4:" + this.word + "→" + originalword;
      }
    }
  }

  muatil_rule5() {
    // Rule 5.1
    // waow, dhamma
    const rule51regex = /^\u0648\u064F/;
    var match51 = rule51regex.exec(this.word);
    if (match51) {
      const originalword51 = this.word;
      this.word = replaceAt(this.word, 0, "\u0623");
      this.word = replaceAt(this.word, 1, "\u064F");
      this.matchpath += "|Mu5.1:" + this.word + "→" + originalword51;
    }
    // Rule 5.2 not implemented
    const rule52regex = /^\u0648\u0650/;
    var match52 = rule52regex.exec(this.word);

    if (match52) {
      const originalword52 = this.word;
      this.word = replaceAt(this.word, 0, "\u0625");
      this.matchpath += "|Mu5.2:" + this.word + "→" + originalword52;
    }
  }

  muatil_rule6() {
    const rule6regex = /^\u0648[\u064E-\u0650]\u0648[\u064E-\u0650]/;

    var match6 = rule6regex.exec(this.word);
    if (match6) {
      const originalword = this.word;
      this.word = replaceAt(this.word, 0, "\u0623");
      this.matchpath += "|Mu6.2:" + this.word + "→" + originalword;
    }
  }

  muatil_rule7() {
    // Fatha, waow/yeh, fatha
    // This was harder to write, don't ask me to explain it!
    // Not starting with Hamza, fatha, ya/waow, then fatha + ending or  ending or fatha
    const rule7regex = /[^\u0623-\u0626]\u064E([\u064A\u0648])((\u064E$|$)|\u064E)/;

    var match7 = rule7regex.exec(this.word);
    if (match7) {

      // Reject matches if certain conditions are met
      // This regex does not match the exception condition #1
      for (const q in this.faa_locations) {
        //const offset = this.faa_locations[q];
        // The offset of 2: 0=any letter, 1=haraka, 2=waow/yeh
        if (this.faa_locations[q] == match7.index + 2) {
          return; // Return the original value
        }
      }

      const weakletters = /[\u0627\u0648\u064A]/;
      // Exception condition #2)
      if (
        (weakletters.exec(this.faa) && (weakletters.exec(this.ayn) || weakletters.exec(this.lam))) ||
        (weakletters.exec(this.ayn) && (weakletters.exec(this.lam)))
        ) {
        return;
      }

      // Exception condition #3 not currently implemented
      // Exception condition #4 not currently implemented

      const originalword = this.word;
      if (match7[1] == "\u064A" && match7[3] !== undefined) {
        if (match7[2] != "") { // Get rid of the trailing fatha
          this.word = deleteAt(this.word, match7.index+3);
        }
        this.word = replaceAt(this.word, match7.index+2, "\u0649");

      } else {
        if (match7[2] != "") { // Get rid of the trailing fatha
          this.word = deleteAt(this.word, match7.index+3);
        }
        this.word = replaceAt(this.word, match7.index+2, "\u0627");
      }
      this.matchpath += "|Mu7:" + this.word + "→" + originalword;
    }
  }

  muatil_rule8() {
    // Sukoon, waow/yeh,
    const rule8regex = /\u0652[\u0648\u064A]([\u064E-\u0650])/;
    const match8 = rule8regex.exec(this.word);

    if (match8) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match8.index+2, "\u0652");
      this.word = replaceAt(this.word, match8.index, match8[1]);
      this.matchpath += "|Mu8:" + this.word + "→" + originalword;

      // Mid rule that does not seem to be explicitly mentioned (or I'm missing it)
      const rule8midregex = /\u064E[\u0648\u064A]\u0652/;
      const match8mid = rule8midregex.exec(this.word);
      if (match8mid) {
        const midmatchword = this.word;
        this.word = replaceAt(this.word, match8mid.index+1, "\u0627");
        this.word = deleteAt(this.word, match8mid.index+2);
        this.matchpath += "|Mu8mid:" + this.word + "→" + midmatchword;
      }
    }
  }

  muatil_rule9() {
    // 9.1
    if ((this.ayn == WAOW || this.ayn==YAH) && this.mushtaq == "majhul_madi") {// Waow
      const originalword = this.word;

      const faa_haraka_location = this.faa_locations[0] + 1;
      const ayn_haraka_location = this.ayn_locations[0] + 1;

      this.word = replaceAt(this.word, faa_haraka_location, this.word[ayn_haraka_location]);
      this.word = replaceAt(this.word, ayn_haraka_location, SUKOON);
      this.word = replaceAt(this.word, this.ayn_locations[0], YAH);

      this.matchpath += "|Mu9:" + this.word + "→" + originalword;

    }
    // 9.5
  }

  muatil_rule10() {
    if (this.form == "fataha" || this.form == "samia") {
      if (this.mushtaq == "hazir_mudari" && this.lam == WAOW) {
        const originalword = this.word;
        const lam_location = this.lam_locations[0];

        // Only Kasra condition requires changing the waow
        // The reason for the second verification is that the letter hasn't changed.
        if (this.word[lam_location] == WAOW && this.word[lam_location-1] == KASRA) {
          this.word = replaceAt(this.word, lam_location, ALIFMAQSURA);
        }

        this.word = insertAt(this.word, lam_location+1, SUKOON);
        this.munsarif = "ghayrmunsarif";
        this.matchpath += "|Mu10.1:" + this.word + "→" + originalword;
      }

      // The other subrules are for plurality, which is currently not implemented.
    }
  }

  muatil_rule11() {
    if (this.lam == WAOW) {
      const lam_location = this.lam_locations[0];
      // The reason for the second verification is that the letter hasn't changed.
      if (this.word[lam_location] == WAOW && this.word[lam_location-1] == KASRA) {
        const originalword = this.word;
        this.word = replaceAt(this.word, lam_location, YAH);
        this.matchpath += "|Mu11:" + this.word + "→" + originalword;
      }
    }
  }

  muatil_rule12() {
    if (this.lam == YAH) {
      if (!this.lam_locations) {
        return;
      }
      const lam_location = this.lam_locations[0];
      // The reason for the second verification is that the letter hasn't changed.
      if (this.word[lam_location] == YAH && this.word[lam_location-1] == DHAMMA) {
        const originalword = this.word;
        this.word = replaceAt(this.word, lam_location, WAOW);
        this.matchpath += "|Mu12:" + this.word + "→" + originalword;
      }
    }
  }

  muatil_rule13() {
    if (this.ayn == WAOW) {
      const ayn_location = this.ayn_locations[0];
      // The reason for the second verification is that the letter hasn't changed.
      if (this.word[ayn_location] == WAOW && this.word[ayn_location-1] == KASRA) {
        const originalword = this.word;
        this.word = replaceAt(this.word, ayn_location, YAH);
        this.matchpath += "|Mu13:" + this.word + "→" + originalword;
      }
    }
  }

  muatil_rule15() {
    // Checks if matches bab FuAuuL
    const regexrule15 = /^([\u0627-\u063A\u0641-\u0648\u064A])\u064F([\u0627-\u063A\u0641-\u0648\u064A])\u064F?\u0648\u0652?([\u0627-\u063A\u0641-\u0648\u064A])[\u064B-\u0650]?$/;

    const match15 = regexrule15.exec(this.word);

    if (match15 && match15[3] == WAOW) {
      const originalword = this.word;
      // The offsets below are fixed due to the form type.
      this.word = replaceAt(this.word, 4, YAH);
      this.word = replaceAt(this.word, 6, YAH);
      this.matchpath += "|Mu15:" + this.word + "→" + originalword;
    }
  }

  muatil_rule16() {
    if (this.lam == WAOW) {
      if (this.mushtaq == "hazir_masdar" || this.mushtaq == "hazir_fail" || this.mushtaq == "majhul_masdar" || this.mushtaq == "majhul_fail" || this.mushtaq == "zarf" || this.mushtaq == "aalat" || this.mushtaq == "afala_muzakir" || this.mushtaq == "afala_muanath") {
        const originalword = this.word;
        let lam_location = this.lam_locations[0];
        if (this.word[lam_location-1] == DHAMMA) {
          this.word = replaceAt(this.word, lam_location-1, KASRA);
          this.word = replaceAt(this.word, lam_location, YAH);
          this.matchpath += "|Mu16.1:" + this.word + "→" + originalword;
        }
      }
    }

    const regexrule163 = /\u064F\u0648\u064C$/;
    if (this.mushtaq == "hazir_masdar" && this.form == "taqaabala") {
      let matchrule163 = regexrule163.exec(this.word);
      if (matchrule163) {
        const originalword2 = this.word;
        this.word = replaceAt(this.word, matchrule163.index+1, YAH);
        this.matchpath += "|Mu16.2:" + this.word + "→" + originalword2;
      }
    }
  }

  muatil_rule17() {
    if (this.mushtaq == "hazir_fail" && this.ayn == WAOW && (this.form == "nasara" || this.form == "fataha" || this.form == "hasiba" || this.form == "samia" || this.form == "daraba" || this.form == "karuma")) {
      let ayn_location = this.ayn_locations[0];
      if (this.word[ayn_location] == WAOW) {
        const originalword = this.word;
        let ayn_location = this.ayn_locations[0];
        this.word = replaceAt(this.word, ayn_location, HAMZAYAH);
        this.matchpath += "|Mu17:" + this.word + "→" + originalword;
      }
    }
  }

  muatil_rule18() {
    // Any letter, fatha, any letter, fatha, alif, waow/alif/yeh, kasra, any letter
    const regexrule18 = /^[\u0628-\u064A]\u064E[\u0628-\u064A]\u064E\u0627([\u0627\u0648\u064A])\u0650[\u0628-\u064A]/;
    const match18 = regexrule18.exec(this.word);

    if (match18) {
      const originalword = this.word;
      this.word = replaceAt(this.word, 5, HAMZAYAH);
      this.matchpath += "|Mu18:" + this.word + "→" + originalword;
    }
  }

  muatil_rule19() {
    if (this.lam == YAH || this.lam == WAOW) {
      let lam_location = this.lam_locations[0];
      if ( this.word[lam_location-1] == ALIF) {
        const originalword = this.word;
        this.word = replaceAt(this.word, lam_location, HAMZA);
        this.matchpath += "|Mu19:" + this.word + "→" + originalword;
      }
    }
    return;
  }

  /*
  function muatil_rule20(wordset: { [x: string]: any; }, this.form: string, this.mushtaq: string, this.faa: string, this.ayn: string, this.lam: string) {
    if (this.mushtaq.includes("madi") || this.mushtaq.includes("mudari") || this.mushtaq.includes("amr") || this.mushtaq.includes("nahi")) {
      for(let q = 6; q < this.word.length; q++) {
        if (this.word[q] == WAOW && (
          this.word[q] != DHAMMA ||
          !(this.word[q-1] == SUKOON && this.word[q-1] == WAOW)
          )
        ) {
          this.word = replaceAt(this.word, q, YAH);
        }
      }
    }
    return wordset;
    //return wordset;
  }
  */

  muatil_rule21() {
    let regexrule211 = /\u064F\u0627/;
    let match211 = regexrule211.exec(this.word);
    if (match211) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match211.index+1, WAOW);
      this.matchpath += "|Mu21.1:" + this.word + "→" + originalword;
    }

    let regexrule212 = /\u0650\u0627/;
    let match212 = regexrule212.exec(this.word);
    if (match212) {
      const originalword = this.word;
      this.word = replaceAt(this.word, match212.index+1, YAH);
      this.matchpath += "|Mu21.2:" + this.word + "→" + originalword;
    }
  }

  muatil_ending1() {
    // This rule might exist elsewhere, but I want to complete it now
    let ending1regex = /\u0650[\u0648\u064A]\u064C$/;
    let match = ending1regex.exec(this.word);

    if (match) {
      const originalword = this.word;
      this.word = deleteAt(this.word, match.index+2);
      this.word = deleteAt(this.word, match.index+1);
      this.word = replaceAt(this.word, match.index, KASRATAYN);
      this.matchpath += "|MuEnding1:" + this.word + "→" + originalword;
    }
  }

  muatil() {
    // Rule 1
    this.muatil_rule1();
    // Rule 2
    // Rule 3
    this.muatil_rule3();
    // Rule 4 - Implemented as an early rule under earlyRules() -> muatil_rule4
    // Rule 5
    this.muatil_rule5()
    // Rule 6 - Implemented as an early rule under earlyRules() -> muatil_rule6
    // Rule 7
    this.muatil_rule7();
    this.muatil_rule8();
    this.muatil_rule9();
    // Rule 10 - Implemented as an early rule under earlyRules() -> muatil_rule10
    this.muatil_rule11();
    this.muatil_rule12();
    this.muatil_rule13();
    // Rule 14 currently unimplemented
    this.muatil_rule15();
    this.muatil_rule16();
    this.muatil_rule17();
    this.muatil_rule18();
    this.muatil_rule19();
    //  wordset = muatil_rule20(wordset, this.form, this.mushtaq, this.faa, this.ayn, this.lam);
    this.muatil_rule21();

    this.muatil_ending1();

  }

  earlyRules() {
    this.muatil_rule4();
    this.muatil_rule6();
    this.muatil_rule10();
  }
  
  idgham() {
    const idghamregex = /([\u0627-\u064A])\u0652\1[\u064B-\u0650]/;

    var match = idghamregex.exec(this.word);
    if (match) {
      const originalword = this.word;
      this.word = deleteAt(this.word, match.index);
      this.word = deleteAt(this.word, match.index);
      this.word = insertAt(this.word, match.index+1, "\u0651"); // shadda
      this.matchpath += "|IG:" + this.word + "→" + originalword;
    }
  }


  closingRules() {
    this.idgham();
  }

  talilat() {
    if (this.ruleset == "disabled") {
      return;
    }

    //for (const this.mushtaq in wordsets) {
    this.replaceHamza();
    this.earlyRules();
    this.mahmuz();
    this.muatil();
    this.closingRules();
    //}
  }
}

class WordsetFormTwoSarrafa extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "sarrafa");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Two - Sarrafa
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [2];
        this.lam_locations = [5];
        break;
      case "hazir_mudari":
        this.word = YAH + DHAMMA + this.faa + FATHA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + YAH + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + FATHA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "majhul_madi":
        this.word = this.faa + DHAMMA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [2];
        this.lam_locations = [5];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA  + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_amr_0":
        this.word = this.faa + FATHA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [2];
        this.lam_locations = [5];
        break;
      case "hazir_nahi_0":
        this.word = TAH + DHAMMA + this.faa + FATHA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
    }
  }
}

class WordsetFormThreeQaatala extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "qaatala");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Three - Qaatala
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [3];
        this.lam_locations = [5];
        break;
      case "hazir_mudari":
        this.word = YAH + DHAMMA + this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = MEEM + DHAMMA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam + FATHA + TAHMARBUTA;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        this.gender = false;
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "majhul_madi":
        this.word = this.faa + DHAMMA + WAOW + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "hazir_amr_0":
        this.word = this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [3];
        this.lam_locations = [5];
        break;
      case "hazir_nahi_0":
        this.word = MEEM + DHAMMA + this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
    }
  }
}

class WordsetFormFourAkrama extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "akrama");
    this.buildword();
    this.talilat();
  }

  buildword() {
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIF + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_mudari":
        this.word = YAH + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "majhul_madi":
        this.word = this.faa + DHAMMA + WAOW + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [0];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "majhul_mudari":
        this.word = HAMZAALIF + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIF + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_nahi_0":
        this.word = TAH + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
    }
  }
}

class WordsetFormFiveTaqabbala extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "taqabbala");
    this.buildword();
    this.talilat();
  }

  buildword() {
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = TAH + FATHA + this.faa + FATHA + this.ayn + SHADDA + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + TAH + FATHA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = TAH + FATHA + this.faa + FATHA + this.ayn + DHAMMA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + TAH + FATHA + this.faa + FATHA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "majhul_madi":
        this.word = TAH + DHAMMA + this.faa + DHAMMA + this.ayn + KASRA + SHADDA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + TAH + FATHA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + TAH + FATHA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "hazir_amr_0":
        this.word = TAH + FATHA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + this.faa + FATHA + this.ayn + FATHA + SHADDA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
    }
  }
}

class WordsetFormSixTaqaabala extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "taqaabala");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Five - Taqabbala
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [7];
        this.lam_locations = [9];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + DHAMMA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [7];
        this.lam_locations = [9];
        break;
      case "majhul_madi":
        this.word = TAH + DHAMMA + this.faa + DHAMMA + WAOW + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [7];
        this.lam_locations = [9];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [7];
        this.lam_locations = [9];
        break;
      case "hazir_amr_0":
        this.word = TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [5];
        this.lam_locations = [7];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + TAH + FATHA + this.faa + ALIF + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [7];
        this.lam_locations = [9];
        break;
    }
  }
}

class WordsetFormSevenInfatara extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "infatara");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Five - Taqabbala
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + NUN + SUKOON + this.faa + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + NUN + SUKOON + this.faa + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + KASRA + NUN + SUKOON + this.faa + KASRA + this.ayn + FATHA + ALIF + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + NUN + SUKOON + this.faa + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_madi":
        this.word = HAMZAALIF + DHAMMA + NUN + SUKOON + this.faa + DHAMMA + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + NUN + SUKOON + this.faa + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + NUN + SUKOON + this.faa + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + NUN + SUKOON + this.faa + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + NUN + SUKOON + this.faa + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [4];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
    }
  }
}

class WordsetFormEightIjtanaba extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "ijtanaba");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Five - Taqabbala
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + TAH + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + this.faa + SUKOON + TAH + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + TAH + KASRA + this.ayn + FATHA + ALIF + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [9];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + TAH + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_madi":
        this.word = HAMZAALIF + DHAMMA + this.faa + SUKOON + TAH + DHAMMA + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + this.faa + SUKOON + TAH + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + TAH + FATHA + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + TAH + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + this.faa + SUKOON + TAH + FATHA + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [6];
        this.lam_locations = [8];
        break;
    }
  }
}

class WordsetFormNineIhmarra extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "ihmarra");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Nine - Ihmarra
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + KASRA + this.lam + FATHA + ALIF + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6, 9];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA + FATHA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        this.munsarif = "ghayrmunsarif";
        break;
      case "hazir_amr_1":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA + KASRA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        this.munsarif = "ghayrmunsarif";
        break;
      case "hazir_amr_2":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + this.lam + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6, 8];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA + FATHA;
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA + FATHA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        this.munsarif = "ghayrmunsarif";
        break;
      case "hazir_nahi_1":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam + SHADDA + KASRA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6];
        this.munsarif = "ghayrmunsarif";
        break;
      case "hazir_nahi_2":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [6, 8];
        break;
    }
  }
}

class WordsetFormTenIstansara extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "istansara");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Nine - Istansara
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "hazir_mudari":
        this.word = YAH + KASRA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + FATHA + SIN + SUKOON + TAH + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [11];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + SIN + SUKOON + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [9];
        this.lam_locations = [11];
        break;
      case "majhul_madi":
        this.word = HAMZAALIF + DHAMMA + SIN + SUKOON + TAH + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + SIN + SUKOON + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [9];
        this.lam_locations = [11];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
    }
  }
}

class WordsetFormIkhshawshana extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "ikhshawshana");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Ikhshawshana
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4, 8];
        this.lam_locations = [10];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4, 8];
        this.lam_locations = [10];
        break;
      case "hazir_masdar":
      case "majhul_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + KASRA + YAH + SUKOON + this.ayn + FATHA + ALIF +  this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4, 8];
        this.lam_locations = [11];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4, 8];
        this.lam_locations = [10];
        break;
      case "majhul_madi":
        this.word = HAMZAALIF + DHAMMA + SIN + SUKOON + TAH + DHAMMA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "majhul_mudari":
        this.word = YAH + DHAMMA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "majhul_fail":
        this.word = MEEM + DHAMMA + SIN + SUKOON + TAH + FATHA + this.faa + FATHA + ALIF + this.ayn + FATHA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [9];
        this.lam_locations = [11];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + SIN + SUKOON + TAH + FATHA + this.faa + SUKOON + this.ayn + KASRA + this.lam;
        this.faa_locations = [6];
        this.ayn_locations = [8];
        this.lam_locations = [10];
        break;
    }
  }
}

class WordsetFormIdhaamma extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "idhaamma");
    this.buildword();
    this.talilat();
  }

  buildword() {
    // Form Idhaamma
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + KASRA + YAH + SUKOON + this.ayn + FATHA + ALIF + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4, 8];
        this.lam_locations = [11];
        break;
      case "hazir_fail":
        this.word = MEEM + DHAMMA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_amr_0":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA + FATHA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_amr_1":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA + KASRA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        this.munsarif = "ghayrmunsarif";
        break;
      case "hazir_amr_2":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7, 9];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA + FATHA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_nahi_1":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + SHADDA + KASRA;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7];
        break;
      case "hazir_nahi_2":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + ALIF + this.lam + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [7, 9];
        break;
    }
  }
}


class WordsetFormIjlawwadha extends WordsetBase {
  constructor(faa: string, ayn: string, lam: string, mushtaq: string) {
    super(faa, ayn, lam, mushtaq, "ijlawwadha");
    this.buildword();
    this.talilat();
  }

  buildword() {
    switch(this.mushtaq) {
      case "hazir_madi":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SHADDA + FATHA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [9];
        break;
      case "hazir_mudari":
        this.word = YAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SHADDA + KASRA + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [9];
        break;
      case "hazir_masdar":
        this.word = HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + KASRA + WAOW + SHADDA + FATHA + ALIF + this.lam;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [10];
        break;
      case "hazir_fail":
        this.word = MEEM + this.faa + SUKOON + this.ayn + FATHA + WAOW + SHADDA + KASRA + this.lam + SHADDA;
        this.faa_locations = [1];
        this.ayn_locations = [3];
        this.lam_locations = [8];
        break;
      case "hazir_amr_0":
        this.word =HAMZAALIFBOT + KASRA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SHADDA + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [9];
        break;
      case "hazir_nahi_0":
        this.word = TAH + FATHA + this.faa + SUKOON + this.ayn + FATHA + WAOW + SHADDA + KASRA + this.lam + SUKOON;
        this.faa_locations = [2];
        this.ayn_locations = [4];
        this.lam_locations = [9];
        break;
    }
  }
}

function gardanWordEndings(mushtaqat: Record<string, any>) {
  for (const form in mushtaqat) {
    for (const mushtaq in mushtaqat[form]) {
      if (mushtaqat[form][mushtaq].munsarif == "munsarif") {
        const wordlen = mushtaqat[form][mushtaq].word.length;
        if (mushtaq.includes("madi")) {
          mushtaqat[form][mushtaq].word = insertAt(mushtaqat[form][mushtaq].word, wordlen+1, FATHA);
        } else if (mushtaq.includes("mudari")) {
          mushtaqat[form][mushtaq]["word"] = insertAt(mushtaqat[form][mushtaq].word, wordlen+1, DHAMMA);
        } else if (mushtaq.includes("masdar")) {
          mushtaqat[form][mushtaq].word = insertAt(mushtaqat[form][mushtaq].word, wordlen+1, FATHATAYN);
          if (mushtaqat[form][mushtaq]["gender"] == true)  { // Masculine
            mushtaqat[form][mushtaq].word = insertAt(mushtaqat[form][mushtaq].word, wordlen+2,  ALIF);
          }
        } else if (mushtaq.includes("amr") || mushtaq.includes("nahi")) {
          mushtaqat[form][mushtaq].word = insertAt(mushtaqat[form][mushtaq].word, wordlen+1, SUKOON);
        } else if (mushtaq.includes("fail")) {
          mushtaqat[form][mushtaq].word = insertAt(mushtaqat[form][mushtaq].word, wordlen+1, DHAMMATAYN);
        }
      }
    }
  }
  return mushtaqat;
}


export function doSarf(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaqat: Record <string, any> = {};
  mushtaqat["sarrafa"]      = sarfformTwo_sarrafa(faa, ayn, lam);
  mushtaqat["qaatala"]      = sarfformThree_qaatala(faa, ayn, lam);
  mushtaqat["akrama"]       = sarfformFour_akrama(faa, ayn, lam);
  mushtaqat["taqabbala"]    = sarfformFive_taqabbala(faa, ayn, lam);
  mushtaqat["taqaabala"]    = sarfformSix_taqaabala(faa, ayn, lam);
  mushtaqat["infatara"]     = sarfformSeven_infatara(faa, ayn, lam);
  mushtaqat["ijtanaba"]     = sarfformEight_ijtanaba(faa, ayn, lam);
  mushtaqat["ihmarra"]      = sarfformNine_ihmarra(faa, ayn, lam);
  mushtaqat["istansara"]    = sarfformTen_istansara(faa, ayn, lam);
  mushtaqat["ikhshawshana"] = sarfform_ikhshawshana(faa, ayn, lam);
  mushtaqat["idhaamma"]     = sarfform_idhaamma(faa, ayn, lam);
  mushtaqat["ijlawwadha"]   = sarfform_ijlawwadha(faa, ayn, lam);

  mushtaqat = gardanWordEndings(mushtaqat);
  return mushtaqat
}

export function sarfformTwo_sarrafa(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormTwoSarrafa(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormTwoSarrafa(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormTwoSarrafa(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormTwoSarrafa(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormTwoSarrafa(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}


export function sarfformThree_qaatala(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormThreeQaatala(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormThreeQaatala(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormThreeQaatala(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormThreeQaatala(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormThreeQaatala(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformFour_akrama(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormFourAkrama(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormFourAkrama(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormFourAkrama(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormFourAkrama(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormFourAkrama(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformFive_taqabbala(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormFiveTaqabbala(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormFiveTaqabbala(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormFiveTaqabbala(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormFiveTaqabbala(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormFiveTaqabbala(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformSix_taqaabala(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormSixTaqaabala(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormSixTaqaabala(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormSixTaqaabala(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormSixTaqaabala(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormSixTaqaabala(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformSeven_infatara(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormSevenInfatara(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormSevenInfatara(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormSevenInfatara(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormSevenInfatara(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormSevenInfatara(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformEight_ijtanaba(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormEightIjtanaba(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormEightIjtanaba(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormEightIjtanaba(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormEightIjtanaba(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormEightIjtanaba(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfformNine_ihmarra(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_amr_1"]   = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_amr_1");
  mushtaq["hazir_amr_2"]   = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_amr_2");
  mushtaq["hazir_nahi_0"]  = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_nahi_0");
  mushtaq["hazir_nahi_1"]  = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_nahi_1");
  mushtaq["hazir_nahi_2"]  = new WordsetFormNineIhmarra(faa, ayn, lam, "hazir_nahi_2");

  return mushtaq;
}

export function sarfformTen_istansara(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormTenIstansara(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormTenIstansara(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormTenIstansara(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormTenIstansara(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormTenIstansara(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfform_ikhshawshana(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_fail");
  mushtaq["majhul_madi"]   = new WordsetFormIkhshawshana(faa, ayn, lam, "majhul_madi");
  mushtaq["majhul_mudari"] = new WordsetFormIkhshawshana(faa, ayn, lam, "majhul_mudari");
  mushtaq["majhul_masdar"] = new WordsetFormIkhshawshana(faa, ayn, lam, "majhul_masdar");
  mushtaq["majhul_fail"]   = new WordsetFormIkhshawshana(faa, ayn, lam, "majhul_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormIkhshawshana(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfform_idhaamma(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormIdhaamma(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}

export function sarfform_ijlawwadha(faa: string, ayn: string, lam: string): Record <string, any> {
  let mushtaq: Record <string, any> = {};

  mushtaq["hazir_madi"]    = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_madi");
  mushtaq["hazir_mudari"]  = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_mudari");
  mushtaq["hazir_masdar"]  = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_masdar");
  mushtaq["hazir_fail"]    = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_fail");
  mushtaq["hazir_amr_0"]   = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_amr_0");
  mushtaq["hazir_nahi_0"]  = new WordsetFormIjlawwadha(faa, ayn, lam, "hazir_nahi_0");

  return mushtaq;
}



